package org.eclipse.epsilon.evl.parallel;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.eclipse.epsilon.evl.parallel.execute.EvlModuleParallelStatus;

public class EvlThreadPoolExecutor extends ThreadPoolExecutor {
	
	private EvlModuleParallelStatus status;

	public EvlThreadPoolExecutor(int corePoolSize,int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue,
			ThreadFactory threadFactory, EvlModuleParallelStatus status) {
		
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
				threadFactory);
		
		this.status = status;
	}

	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		
		if (t != null)
			status.addException(Thread.currentThread(), t);
	}
}
