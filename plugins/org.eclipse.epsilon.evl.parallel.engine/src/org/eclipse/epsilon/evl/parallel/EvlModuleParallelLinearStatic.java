package org.eclipse.epsilon.evl.parallel;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.eclipse.epsilon.common.parse.AST;
import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.dom.ExecutableBlock;
import org.eclipse.epsilon.eol.dom.Expression;
import org.eclipse.epsilon.eol.dom.OperationCallExpression;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.execute.context.Variable;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.dom.ConstraintContext;
import org.eclipse.epsilon.evl.execute.EvlOperationFactory;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;
import org.eclipse.epsilon.evl.parallel.execute.EvlModuleParallelStatus;
import org.eclipse.epsilon.evl.parallel.execute.context.IEvlContextParallel;

public class EvlModuleParallelLinearStatic extends EvlModuleParallel {
	
	public static void main(String[] args) throws Exception {
		
		String resultsDir = "D:\\Google Drive\\Project\\testing\\parallel\\debug\\";
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HHmmss");
		
		String date = dateFormat.format(new Date());
		
		String resultsFile = resultsDir + date + " EvlModuleParallelLinear results.txt";
		new File(resultsFile).createNewFile();
		PrintWriter ro = new PrintWriter(resultsFile);
		
		
		EmfModel emfModel = new EmfModel();
		StringProperties properties = new StringProperties();
		properties.put(EmfModel.PROPERTY_NAME, "Model");
		properties.put(EmfModel.PROPERTY_FILE_BASED_METAMODEL_URI,
				new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\Y3586707.ecore").toURI().toString());
		properties.put(EmfModel.PROPERTY_MODEL_URI, 
				new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\example.model").toURI().toString());
		properties.put(EmfModel.PROPERTY_READONLOAD, true + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
				true + "");
		properties.put(EmfModel.PROPERTY_CACHED, true + "");
		
		
		int mb = 1024 * 1024;
		
		Runtime r = Runtime.getRuntime();
		 
		System.out.println("Total memory: " + (r.totalMemory() / mb));
		System.out.println("Free memory: " + (r.freeMemory() / mb));
		System.out.println("Max memory: " + (r.maxMemory() / mb));
		
		
		
		emfModel.load(properties, (IRelativePathResolver) null);
		
		EvlModuleParallelLinearStatic module = new EvlModuleParallelLinearStatic();
		module.setNumThreads(2);
		module.parse(new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\Y3586707.evl"));
		
		module.getContext().getModelRepository().addModel(emfModel);
		
		module.execute();
		
		//post process
		Collection<UnsatisfiedConstraint> unsatisfied = module.getContext().getUnsatisfiedConstraints();
		
		if (unsatisfied.size() > 0) {
			System.err.println(unsatisfied.size() + " constraint(s) have not been satisfied");
			for (UnsatisfiedConstraint uc : unsatisfied) {
				ro.append(uc.getMessage() + "\r\n");
				System.err.println(uc.getMessage());
			}
		}
		else {
			System.out.println("All constraints have been satisfied");
		}
		
		ro.close();
	}
	
	static final String satisfies = "satisfies";
	static final String guard = "guard";
	static final String check = "check";
	static final String message = "message";
	
	protected static boolean hasDependencies(AST ast) {
		List<AST> children = ast.getChildren();
		for (AST child : children)
			if (child instanceof ExecutableBlock<?>) {
				String text = child.getText();
				if (text.equals(guard) || text.equals(check) || text.equals(message)) {
					for (AST blockChild : child.getDescendants())
						if (blockChild instanceof OperationCallExpression)
							if (((OperationCallExpression) blockChild).getOperationName().equals(satisfies))
								return true;
				}
			}
		return false;
	}
	
	@Override
	public Object execute() throws EolRuntimeException {
		// Initialise the context
		prepareContext(context);
		context.setOperationFactory(new EvlOperationFactory());
		context.getFrameStack().put(Variable.createReadOnlyVariable("constraintTrace", context.getConstraintTrace()));
		context.getFrameStack().put(Variable.createReadOnlyVariable("thisModule", this));
		
		execute(getPre(), context);
		
//		System.out.println("# Threads: " + numThreads);
		final EvlModuleParallelStatus contextStatus = new EvlModuleParallelStatus();		
		final EvlModuleParallelThreadFactory factory = new EvlModuleParallelThreadFactory((IEvlContextParallel) context, null);
		((IEvlContextParallel) context).goParallel();
		
		//Process context-level guard block
		final EvlThreadPoolExecutor contextJobExecutor = new EvlThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new LinkedBlockingQueue<Runnable>(), factory, contextStatus); 
		
		for (final ConstraintContext constraintContext : getConstraintContexts()) {
//			System.out.println("Adding context: " + constraintContext);
			Collection allOfKind = constraintContext.getAllOfSourceKind(context);
			for (final Object object : allOfKind)
				contextJobExecutor.execute(getContextRunnable(constraintContext, object, contextStatus));
		}
		
//		System.out.println("Waiting");
		
		if (!waitForJobsSuccessful(contextStatus, contextJobExecutor))
			throw new EolRuntimeException("Error in job thread - aborting");
		
//		System.out.println("Finished Waiting");

		//Process constraint-level guard block & check block
		final EvlModuleParallelStatus constraintStatus = new EvlModuleParallelStatus();
		final EvlPriorityThreadPoolExecutor constraintJobExecutor = new EvlPriorityThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new PriorityBlockingQueue<Runnable>(), factory, contextStatus);
		int constraintCount = 0;
		int contextJobCount = 0;
		
		ArrayList<Object[]> lowPriorityConstraintJobs = new ArrayList<Object[]>();
		HashMap<Constraint, Boolean> constraintsUseSatisfies = new HashMap<Constraint, Boolean>();
		
//		System.out.println("Context jobs: " + contextJobs.size());
		for (final Object[] job : contextJobs) {
			for (final Constraint constraint : ((ConstraintContext) job[0]).getConstraints()) {
				if (!constraint.isLazy(context)) {
//					System.out.println("Adding constraint: " + constraint);
					Boolean usesSatisfies = constraintsUseSatisfies.get(constraint);
					if (usesSatisfies == null) {
						usesSatisfies = hasDependencies(constraint);
						constraintsUseSatisfies.put(constraint, usesSatisfies);
					}
					if (!usesSatisfies)
						constraintJobExecutor.execute(getConstraintRunnable(constraint, job[1], constraintStatus), 1);
					else {
//						System.out.println("Deferring constraint: " + constraint);
						lowPriorityConstraintJobs.add(new Object[] { constraint, job[1] });
					}
				}
			}
		}
		
		for (final Object[] job : lowPriorityConstraintJobs) {
//			System.out.println("Adding deferred constraint: " + job[0]);
			constraintJobExecutor.execute(getConstraintRunnable((Constraint) job[0], job[1], constraintStatus), 2);
		}
		
		if (!waitForJobsSuccessful(constraintStatus, constraintJobExecutor))
			throw new EolRuntimeException("Error in job thread");

		((IEvlContextParallel) context).endParallel();
		
		if (fixer != null) {
			fixer.fix(this);
		}
		
		execute(getPost(), context);
		
		return null;
	}

	protected Runnable getConstraintRunnable(final Constraint constraint, final Object object,
			final EvlModuleParallelStatus constraintStatus) {
		return new Runnable() {
			
			@Override
			public void run() {
//				System.out.println("Running constraint: " + constraint);
				try {
					if (/*!constraint.isLazy(context) && */constraint.appliesTo(object, context)) {
						constraint.check(object, context);
					}
				} catch (EolRuntimeException e) {
//					System.err.println("Error running thread:\n" + constraint + 
//						"\n" + object + "\n" + e.getMessage());
					constraintStatus.addException(Thread.currentThread(), e);
				}
			}
		};
	}
	
	protected Runnable getContextRunnable(final ConstraintContext constraintContext,
			final Object object, final EvlModuleParallelStatus contextStatus) {
		return new Runnable() {
			@Override
			public void run() {
//				System.out.println("Running context: " + constraintContext);
				try {
					if (constraintContext.appliesTo(object, context)) {
//						System.out.println("Adding context job: " + constraintContext + ", " + object);
						synchronized (contextJobs) {
							contextJobs.add(new Object[] { constraintContext, object } );
						}
					}
				} catch (EolRuntimeException e) {
//					System.err.println("Error running thread:\n" + constraintContext + 
//							"\n" + object + "\n" + e.getMessage());
					contextStatus.addException(Thread.currentThread(), e);
				}
			}
		};
	}
}
