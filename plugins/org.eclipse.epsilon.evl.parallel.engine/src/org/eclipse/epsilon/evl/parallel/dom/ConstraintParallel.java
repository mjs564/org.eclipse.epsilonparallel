package org.eclipse.epsilon.evl.parallel.dom;

import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.execute.context.FrameType;
import org.eclipse.epsilon.eol.execute.context.Variable;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.dom.Fix;
import org.eclipse.epsilon.evl.execute.FixInstance;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;
import org.eclipse.epsilon.evl.execute.context.IEvlContext;
import org.eclipse.epsilon.evl.parallel.execute.UnsatisfiedConstraintParallel;
import org.eclipse.epsilon.evl.parallel.execute.context.EvlContextParallel;
import org.eclipse.epsilon.evl.parallel.execute.context.IEvlContextParallel;

public class ConstraintParallel extends Constraint {
	
@Override
public boolean check(Object self, IEvlContext context) throws EolRuntimeException {
		
		// First look in the trace
		if (context.getConstraintTrace().isChecked(this,self)){
			return context.getConstraintTrace().isSatisfied(this,self);
		}
		
//		System.out.println(Thread.currentThread().getName() + " before appliesTo constraint: " + this + "; object: " + self);
		if (!appliesTo(self,context)) {
			context.getConstraintTrace().addChecked(this,self,false);
			return false;
		}
//		System.out.println(Thread.currentThread().getName() + " after appliesTo constraint: " + this + "; object: " + self);
		
		context.getFrameStack().enterLocal(FrameType.UNPROTECTED, checkBlock.getBody());
		context.getFrameStack().put(Variable.createReadOnlyVariable("self", self));
		if (!checkBlock.execute(context, false)){
			
			UnsatisfiedConstraintParallel unsatisfiedConstraint = new UnsatisfiedConstraintParallel();
			unsatisfiedConstraint.setInstance(self);
			unsatisfiedConstraint.setConstraint(this);

			for (Fix fix : fixes) {
				if (!fix.appliesTo(self, context)) continue;
				FixInstance fixInstance = new FixInstance(context);
				fixInstance.setFix(fix);
				fixInstance.setSelf(self);
				unsatisfiedConstraint.getFixes().add(fixInstance);
			}

			String messageResult = null;
			
			if (messageBlock != null) {
				messageResult = messageBlock.execute(context, false);
			}
			else {
				messageResult = "Invariant " + this.getName() + " failed for " + 
					context.getPrettyPrinterManager().toString(self);
			}
			
			unsatisfiedConstraint.setMessage(messageResult);
			
//			System.out.println(Thread.currentThread().getName() + " calling addChecked false constraint: " + this + "; object: " + self);
			context.getConstraintTrace().addChecked(this,self,false);
			
			context.getUnsatisfiedConstraints().add(unsatisfiedConstraint);

			// We don't dispose the frame we leave because it may be needed for fix parts
			context.getFrameStack().leaveLocal(checkBlock.getBody(), false);
			return false;
		}
		else {
//			System.out.println(Thread.currentThread().getName() + " calling addChecked true constraint: " + this + "; object: " + self);
			context.getConstraintTrace().addChecked(this,self,true);
			context.getFrameStack().leaveLocal(checkBlock.getBody());
			return true;
		}
		
	}
}
