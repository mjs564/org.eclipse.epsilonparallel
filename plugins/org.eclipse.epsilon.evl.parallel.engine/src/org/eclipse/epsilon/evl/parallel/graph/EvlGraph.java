package org.eclipse.epsilon.evl.parallel.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.epsilon.common.parse.AST;
import org.eclipse.epsilon.eol.dom.ExecutableBlock;
import org.eclipse.epsilon.eol.dom.Expression;
import org.eclipse.epsilon.eol.dom.OperationCallExpression;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.dom.ConstraintContext;
import org.eclipse.epsilon.evl.execute.exceptions.EvlCircularAssumptionException;
import org.eclipse.epsilon.evl.execute.exceptions.EvlConstraintNotFoundException;

public class EvlGraph {
	
	protected static final String satisfies = "satisfies";
	protected static final String guard = "guard";
	protected static final String check = "check";
	protected static final String message = "message";
	
	protected Set<Constraint> allNodes = new LinkedHashSet<Constraint>();
	protected Set<Constraint> noDependencies = new LinkedHashSet<Constraint>();
	protected Set<Constraint> hasDependencies = new LinkedHashSet<Constraint>();
	protected Set<Constraint> allConstraintsDependedOn = new LinkedHashSet<Constraint>();
	protected Set<String> allConstraintsDependedOnString = new LinkedHashSet<String>();
	protected Set<String> constraintsToFind = new LinkedHashSet<String>();
	protected Set<String> constraintsFound = new LinkedHashSet<String>();
	protected HashMap<String, Constraint> constraintStringMap = new HashMap<String, Constraint>();
	protected HashMap<Constraint, Set<String>> constraintDependenciesMap = new HashMap<Constraint, Set<String>>();
	
	public void addConstraintContexts(List<ConstraintContext> constraintContexts)
			throws EvlConstraintNotFoundException {
		
		if (constraintContexts == null)
			return;
		
		for (ConstraintContext constraintContext : constraintContexts) {
			Set<String> contextDependencies = getDependencies(constraintContext);
//			allConstraintsDependedOnString.addAll(contextDependencies);
				
			for (String dependency : contextDependencies) {
				if (!(constraintsFound.contains(dependency)))
					constraintsToFind.add(dependency);
			}
				
			for (Constraint constraint : constraintContext.getConstraints()) {
				allNodes.add(constraint);
				String constraintString = constraint.getText();
				constraintsToFind.remove(constraintString);
				constraintsFound.add(constraintString);
				constraintStringMap.put(constraintString, constraint);
				
				Set<String> constraintDependencies = new LinkedHashSet<String>();
				constraintDependencies.addAll(contextDependencies);
				Set<String> tempDependencies = getDependencies(constraint);
				constraintDependencies.addAll(tempDependencies);
					
				for (String dependency : tempDependencies) 
					if (!(constraintsFound.contains(dependency)))
						constraintsToFind.add(dependency);
				
				constraintDependencies.remove(constraint.getText());
				allConstraintsDependedOnString.addAll(constraintDependencies);
				
				if (constraintDependencies.size() > 0) {
					hasDependencies.add(constraint);
					constraintDependenciesMap.put(constraint, constraintDependencies);
				}
				else
					noDependencies.add(constraint);
			}
		}
		
		if (constraintsToFind.size() > 0)
			throw new EvlConstraintNotFoundException(constraintsToFind.iterator().next(), null);
		
		for (String dependency : allConstraintsDependedOnString) {
			allConstraintsDependedOn.add(constraintStringMap.get(dependency));
		}
	}
	
	public Set<Constraint> getAllConstraintsDependedOn() {
		return allConstraintsDependedOn;
	}
	
	public List<Constraint> getEvlSequence() {
		ArrayList<Constraint> result = new ArrayList<Constraint>();
		
		result.addAll(noDependencies);
		
		int numNodesLast = result.size();
		int numNodesDiff = 1;
		
		while (result.size() < allNodes.size()) {
			numNodesDiff = 1;
			while (numNodesDiff != 0) {
				
				Iterator<Constraint> itConstraint = hasDependencies.iterator();
				while (itConstraint.hasNext()) {
					Constraint constraint = itConstraint.next();
					boolean dependenciesSatisfied = true;
					for (String constraintString : constraintDependenciesMap.get(constraint)) {
						Constraint dependency = constraintStringMap.get(constraintString);
						if (!result.contains(dependency) && !(constraint == dependency)) {
							dependenciesSatisfied = false;
							break;
						}
					}
					if (dependenciesSatisfied) {
						result.add(constraint);
						itConstraint.remove();
					}
				}
				
				numNodesDiff = numNodesLast - result.size();
				numNodesLast = result.size();
			}
			if (result.size() < allNodes.size()) {
				Iterator<Constraint> itConstraint = hasDependencies.iterator();
				Constraint constraint = itConstraint.next();
				result.add(constraint);
				itConstraint.remove();
			}
		}
		
		return result;
	}
	
//	@Override
//	public String toString() {
//		final String nl = "\n"; 
//		return 	"Nodes: " + allNodes.size() + nl +
//				"Constraint (no deps): " + noDependencies.size() + nl +
//				"Constraint (has deps): " + hasDependencies.size();
//				
//	}
	
	public static Set<String> getDependencies(AST ast) {
		Set<String> result = new LinkedHashSet<String>();
		
		List<AST> children = ast.getChildren();
		for (AST child : children)
			if (child instanceof ExecutableBlock<?>) {
				String text = child.getText();
				if (text.equals(guard) || text.equals(check) || text.equals(message)) {
					for (AST blockChild : child.getDescendants())
						if (blockChild instanceof OperationCallExpression)
							if (((OperationCallExpression) blockChild).getOperationName().equals(satisfies))
								for (Expression expression : ((OperationCallExpression) blockChild).getParameterExpressions())
									result.add(expression.getText());
				}
			}
		
		return result;
	}

//	class EvlGraphNode {
//		Set<Constraint> dependencies;
//		
//		public EvlGraphNode() {
//			dependencies = new HashSet<Constraint>();
//		}
//		
//		public void addDependency(Constraint constraint) {
//			dependencies.add(constraint);
//		}
//		
//		public Set<Constraint> getDependencies() {
//			return dependencies;
//		}
//	}
//	
//	class EvlGraphNodeConstraint extends EvlGraphNode {
//		Constraint item;
//		
//		public EvlGraphNodeConstraint(Constraint constraint) {
//			item = constraint;
//		}
//		
//		public Constraint getItem() {
//			return item;
//		}
//	}
//	
//	class EvlGraphNodeConstraintContext extends EvlGraphNode {
//		ConstraintContext item;
//		
//		public EvlGraphNodeConstraintContext(ConstraintContext context) {
//			item = context;
//		}
//		
//		public ConstraintContext getItem() {
//			return item;
//		}
//	}
}
