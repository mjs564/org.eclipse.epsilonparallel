/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Dimitrios Kolovos - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.evl.parallel;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.eclipse.epsilon.common.parse.AST;
import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.execute.context.Variable;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.evl.EvlModule;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.dom.ConstraintContext;
import org.eclipse.epsilon.evl.execute.EvlOperationFactory;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;
import org.eclipse.epsilon.evl.execute.context.EvlContext;
import org.eclipse.epsilon.evl.execute.context.IEvlContext;
import org.eclipse.epsilon.evl.parallel.dom.ConstraintParallel;
import org.eclipse.epsilon.evl.parallel.execute.EvlModuleParallelStatus;
import org.eclipse.epsilon.evl.parallel.execute.context.EvlContextParallel;
import org.eclipse.epsilon.evl.parallel.execute.context.IEvlContextParallel;
import org.eclipse.epsilon.evl.parallel.trace.ConstraintTraceParallel;
import org.eclipse.epsilon.evl.parse.EvlParser;

public class EvlModuleParallel extends EvlModule {
	
	public static void main(String[] args) throws Exception {
		EmfModel emfModel = new EmfModel();
		StringProperties properties = new StringProperties();
		properties.put(EmfModel.PROPERTY_NAME, "Model");
		properties.put(EmfModel.PROPERTY_FILE_BASED_METAMODEL_URI,
				new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\java.ecore").toURI().toString());
		properties.put(EmfModel.PROPERTY_MODEL_URI, 
				new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\Test3_java.xmi").toURI().toString());
		properties.put(EmfModel.PROPERTY_READONLOAD, true + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
				true + "");
		properties.put(EmfModel.PROPERTY_CACHED, true + "");
		
		///Users/matt/Google Drive/Project/modisco/workspace/EpsilonSource/java.ecore
		
		
		
		int mb = 1024 * 1024;
		
		Runtime r = Runtime.getRuntime();
		 
		System.out.println("Total memory: " + (r.totalMemory() / mb));
		System.out.println("Free memory: " + (r.freeMemory() / mb));
		System.out.println("Max memory: " + (r.maxMemory() / mb));
		
		
			
		emfModel.load(properties, (IRelativePathResolver) null);
		
		EvlModuleParallel module = new EvlModuleParallel();
		module.setNumThreads(2);
		module.parse(new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\TestingDependencies2.evl"));
		
		module.getContext().getModelRepository().addModel(emfModel);
		
		module.execute();
		
		//post process
		Collection<UnsatisfiedConstraint> unsatisfied = module.getContext().getUnsatisfiedConstraints();
		
		if (unsatisfied.size() > 0) {
			System.err.println(unsatisfied.size() + " constraint(s) have not been satisfied");
			for (UnsatisfiedConstraint uc : unsatisfied) {
				System.err.println(uc.getMessage());
			}
		}
		else {
			System.out.println("All constraints have been satisfied");
		}
	}
	
	protected int numThreads = 1;
	protected volatile ArrayList<Object[]> contextJobs = new ArrayList<Object[]>();
	protected volatile ArrayList<Object[]> constraintJobs = new ArrayList<Object[]>();
	
	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
		((ConstraintTraceParallel) context.getConstraintTrace()).setMaxThreads(numThreads);
	}
	
	public EvlModuleParallel() {
		reset();
	}
	
	@Override
	public Object execute() throws EolRuntimeException {
		// Initialise the context
		prepareContext(context);
		context.setOperationFactory(new EvlOperationFactory());
		context.getFrameStack().put(Variable.createReadOnlyVariable("constraintTrace", context.getConstraintTrace()));
		context.getFrameStack().put(Variable.createReadOnlyVariable("thisModule", this));
		
		execute(getPre(), context);
		
//		System.out.println("# Threads: " + numThreads);
		final EvlModuleParallelStatus contextStatus = new EvlModuleParallelStatus();		
		final EvlModuleParallelThreadFactory factory = new EvlModuleParallelThreadFactory((IEvlContextParallel) context, null);
		((IEvlContextParallel) context).goParallel();
		
		//Process context-level guard block
		final ExecutorService contextJobExecutor = new EvlThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new LinkedBlockingQueue<Runnable>(), factory, contextStatus); 
		
		for (final ConstraintContext constraintContext : getConstraintContexts()) {
//			System.out.println("Context: " + constraintContext);
			Collection allOfKind = constraintContext.getAllOfSourceKind(context);
//			System.out.println(allOfKind.size());
			
			int count = 0;
			
			final MyCounter mc = new MyCounter();
			
			for (final Object object : allOfKind) {
//				System.out.println("Adding job " + ++count);
				contextJobExecutor.execute(new Runnable() {
					
					@Override
					public void run() {
						//System.out.println("Running job " + mc.increment());
						try {
//							System.out.println(Thread.currentThread().getName() + " running context: " + constraintContext + "; " + object);
							if (constraintContext.appliesTo(object, context)) {
//								System.out.println("Adding context job: " + constraintContext +
//										", " + object);
								synchronized (contextJobs) {
									contextJobs.add(new Object[] { constraintContext, object } );
								}
							}
						} catch (EolRuntimeException e) {
							System.err.println("Error running thread:\n" + constraintContext + 
									"\n" + object + "\n" + e.getMessage());
							contextStatus.addException(Thread.currentThread(), e);
						}
					}
				});
			}
		}
		
		if (!waitForJobsSuccessful(contextStatus, contextJobExecutor))
			throw new EolRuntimeException("Error in job thread - aborting");

//		System.out.println("contextJobExecutor: " + ((EvlThreadPoolExecutor) contextJobExecutor).getActiveCount());
//		System.out.println("\n" + contextJobs.size());
		
		
		//Process constraint-level guard block
		final EvlModuleParallelStatus constraintStatus = new EvlModuleParallelStatus();
		final ExecutorService constraintJobExecutor = new EvlThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new LinkedBlockingQueue<Runnable>(), factory, contextStatus);
//		int constraintCount = 0;
//		int contextJobCount = 0;
//		
//		final MyCounter mc2 = new MyCounter();
		
		for (final Object[] job : contextJobs) {
			//System.out.println("For contextJob " + ++contextJobCount);
			for (final Constraint constraint : ((ConstraintContext) job[0]).getConstraints()) {
				//System.out.println("Adding constraintJob runnable " + ++constraintCount);
				if (!constraint.isLazy(context)) {
					constraintJobExecutor.execute(new Runnable() {
						
						@Override
						public void run() {
	//						System.out.println("Running job " + mc2.increment());
							try {
//								System.out.println(Thread.currentThread().getName() + " running constraint guard: " + constraint + "; " + job[1]);
								if (constraint.appliesTo(job[1], context)) {
									synchronized(constraintJobs) {
	//									System.out.println("Adding constraint job: " + constraint + ", " + job[1]);
										constraintJobs.add(new Object[] { constraint, job[1] } );
									}
								}
							} catch (EolRuntimeException e) {
	//							System.err.println("Error running thread:\n" + constraint + 
	//									"\n" + job[1] + "\n" + e.getMessage());
								constraintStatus.addException(Thread.currentThread(), e);
							}
						}
					});
				}
			}
		}
		
		if (!waitForJobsSuccessful(constraintStatus, constraintJobExecutor))
			throw new EolRuntimeException("Error in job thread");

//		System.out.println("constraintJobExecutor: " + ((EvlThreadPoolExecutor) constraintJobExecutor).getActiveCount());
//		System.out.println("# ConstraintJobs: " + constraintJobs.size());
		
		//Process constraint check block
		final EvlModuleParallelStatus checkStatus = new EvlModuleParallelStatus();
		final ExecutorService checkBlockExecutor = new EvlThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new LinkedBlockingQueue<Runnable>(), factory, contextStatus); 
		for (final Object[] job: constraintJobs) {
			checkBlockExecutor.execute(new Runnable() {
				
				@Override
				public void run() {
//					System.out.println(Thread.currentThread().getName() + " running constraint: " + job[0] + "; " + job[1]);
					try {
						((Constraint) job[0]).check(job[1], context);
					} catch (EolRuntimeException e) {
//						System.err.println("Error running thread:\n" + 
//								job[0] + "\n" + job[1] + "\n" + e.getMessage());
						checkStatus.addException(Thread.currentThread(), e);
					}
				}
			});
		}
		
		if (!waitForJobsSuccessful(checkStatus, checkBlockExecutor))
			throw new EolRuntimeException("Error in job thread");
		
//		System.out.println("checkBlockExecutor: " + ((EvlThreadPoolExecutor) checkBlockExecutor).getActiveCount());
		
		((IEvlContextParallel) context).endParallel();
		
		if (fixer != null) {
			fixer.fix(this);
		}
		
		execute(getPost(), context);
		
		return null;
	}

	protected boolean waitForJobsSuccessful(final EvlModuleParallelStatus status,
			final ExecutorService jobExecutor) {
		jobExecutor.shutdown();
		
		Thread termWait = new Thread(new Runnable() {
			
			@Override
			public void run() {
				jobExecutor.shutdown();
				
				try {
					jobExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
				} catch (InterruptedException e) {
					status.setWaitException();
				}
				
				status.setComplete();
			}
		});
		termWait.setName("AwaitTermination");
		termWait.start();
		
		try {
			status.stop();
		} catch (InterruptedException e1) {
			System.err.println("InterruptedException on main thread wait");
			e1.printStackTrace();
		}
		
		if (status.getExceptionCount() > 0) {
			jobExecutor.shutdownNow();
			System.err.println("Encountered exceptions:");
			for (Throwable t : status.getExceptions())
				System.err.println(t.getMessage());
			return false;
		}
		
		if (status.getWaitException()) {
			System.err.println("InterruptedException while waiting for jobs to complete");
			return false;
		}
		
		return true;
	}
	
	protected IEvlContext[] createThreadContexts(IEvlContext context, int number) {
		IEvlContext[] contexts = new IEvlContext[number];
		
		for (int i = 0; i < number; i++) {
			EvlContext newContext = new EvlContext();
			newContext.setModelRepository(context.getModelRepository());
			prepareContext(newContext);
			context.getFrameStack().put(Variable.createReadOnlyVariable("constraintTrace", context.getConstraintTrace()));
			context.getFrameStack().put(Variable.createReadOnlyVariable("thisModule", this));
			contexts[i] = newContext;
		}
		
		return contexts;
	}
	
	@Override
	public void reset() {
		super.reset();
		context = new EvlContextParallel();
	}
	
	class MyCounter {
		int count = 0;
		
		public int increment() {
			return ++count;
		}
	}
	
	@Override
	public AST adapt(AST cst, AST parentAst) {
		switch (cst.getType()) {
			case EvlParser.CONSTRAINT: return new ConstraintParallel();
			case EvlParser.CRITIQUE: return new ConstraintParallel();
		}
		return super.adapt(cst, parentAst);
	}
}
