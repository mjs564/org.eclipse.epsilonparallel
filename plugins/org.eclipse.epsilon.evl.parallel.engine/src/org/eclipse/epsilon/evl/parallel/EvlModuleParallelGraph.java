/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Dimitrios Kolovos - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.evl.parallel;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.epsilon.common.parse.AST;
import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.dom.ExecutableBlock;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelElementTypeNotFoundException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.eol.execute.context.Variable;
import org.eclipse.epsilon.eol.models.CachedModel;
import org.eclipse.epsilon.eol.models.IModel;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.evl.EvlModule;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.dom.ConstraintContext;
import org.eclipse.epsilon.evl.dom.Fix;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;
import org.eclipse.epsilon.evl.execute.context.EvlContext;
import org.eclipse.epsilon.evl.execute.context.IEvlContext;
import org.eclipse.epsilon.evl.parallel.dom.ConstraintParallelGraph;
import org.eclipse.epsilon.evl.parallel.execute.EvlModuleParallelStatus;
import org.eclipse.epsilon.evl.parallel.execute.EvlParallelOperationFactory;
import org.eclipse.epsilon.evl.parallel.execute.context.EvlContextParallel;
import org.eclipse.epsilon.evl.parallel.execute.context.IEvlContextParallel;
import org.eclipse.epsilon.evl.parallel.graph.EvlGraph;
import org.eclipse.epsilon.evl.parallel.trace.ConstraintTraceParallel;
import org.eclipse.epsilon.evl.parse.EvlParser;

public class EvlModuleParallelGraph extends EvlModule {

	public static void main(String[] args) throws Exception {
		EmfModel emfModel = new EmfModel();
		StringProperties properties = new StringProperties();
		properties.put(EmfModel.PROPERTY_NAME, "Model");
		properties.put(EmfModel.PROPERTY_FILE_BASED_METAMODEL_URI,
				new File("D:\\Google Drive\\Project\\examples\\epsilon workspace new\\examples\\java.ecore").toURI().toString());
		properties.put(EmfModel.PROPERTY_MODEL_URI, 
				new File("D:\\Google Drive\\Project\\examples\\epsilon workspace new\\examples\\SingleExample_java.xmi").toURI().toString());
		properties.put(EmfModel.PROPERTY_READONLOAD, true + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
				true + "");
		properties.put(EmfModel.PROPERTY_CACHED, true + "");
		
		int mb = 1024 * 1024;
		
		Runtime r = Runtime.getRuntime();
		
		System.out.println("Total memory: " + (r.totalMemory() / mb));
		System.out.println("Free memory: " + (r.freeMemory() / mb));
		System.out.println("Max memory: " + (r.maxMemory() / mb));
		
		
		emfModel.load(properties, (IRelativePathResolver) null);
		
		EvlModuleParallelGraph module = new EvlModuleParallelGraph();
		module.setNumThreads(2);
		module.parse(new File("D:\\Google Drive\\Project\\examples\\epsilon workspace new\\examples\\TestingDependencies.evl"));
		
		module.getContext().getModelRepository().addModel(emfModel);
		
		module.execute();
		
		//post process
		Collection<UnsatisfiedConstraint> unsatisfied = module.getContext().getUnsatisfiedConstraints();
		
		if (unsatisfied.size() > 0) {
			System.err.println(unsatisfied.size() + " constraint(s) have not been satisfied");
			for (UnsatisfiedConstraint uc : unsatisfied) {
				System.err.println(uc.getMessage());
			}
		}
		else {
			System.out.println("All constraints have been satisfied");
		}
	}
	
	protected int numThreads = 1;
	
	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
		((ConstraintTraceParallel) context.getConstraintTrace()).setMaxThreads(numThreads);
	}
	
	public EvlModuleParallelGraph() {
		reset();
	}
	
	@Override
	public Object execute() throws EolRuntimeException {
		// Initialise the context
		prepareContext(context);
		context.setOperationFactory(new EvlParallelOperationFactory());
		context.getFrameStack().put(Variable.createReadOnlyVariable("constraintTrace", context.getConstraintTrace()));
		context.getFrameStack().put(Variable.createReadOnlyVariable("thisModule", this));
		
		execute(getPre(), context);
		
		  /////////////////////
		 // Pre-cache model //
		/////////////////////
//		for (IModel model : getContext().getModelRepository().getModels()) {
//			if (model instanceof CachedModel<?> && ((CachedModel<?>) model).isCachingEnabled())
//				model.allContents();
//			
//			if (model instanceof EmfModel) {
//				if (((EmfModel) model).isCachingEnabled()) {
//					String metamodelURI = ((EmfModel) model).getProperties().getProperty(EmfModel.PROPERTY_FILE_BASED_METAMODEL_URI);
//					if (metamodelURI != null && metamodelURI != "") {
//						String[] metamodels = metamodelURI.split(",");
//						precacheModel(model, metamodels);
//					}
//					
//					metamodelURI = ((EmfModel) model).getProperties().getProperty(EmfModel.PROPERTY_METAMODEL_URI);
//					if (metamodelURI != null && metamodelURI != "") {
//						String[] metamodels = metamodelURI.split(",");
//						precacheModel(model, metamodels);
//					}
//				}
//			}
//		}
		
		final EvlModuleParallelStatus contextStatus = new EvlModuleParallelStatus();
		final EvlModuleParallelThreadFactory factory = 
				new EvlModuleParallelThreadFactory((IEvlContextParallel) context, null);
		((IEvlContextParallel) context).goParallel();
		
		final EvlPriorityThreadPoolExecutor contextJobExecutor = new EvlPriorityThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new PriorityBlockingQueue<Runnable>(), factory, contextStatus); 
		
		
		EvlGraph graph = new EvlGraph();
		graph.addConstraintContexts(getConstraintContexts());
//		System.out.println("Deps: " + graph.getAllConstraintsDependedOn());
		((IEvlContextParallel) context).setConstraintsDependedOn(graph.getAllConstraintsDependedOn());
//		System.out.println("Deps: " + ((IEvlContextParallel) context).getConstraintsDependedOn());
		int constraintCount = 0;
		List<Constraint> constraintSequence = graph.getEvlSequence();
		for (final Constraint constraint : constraintSequence) {
//			System.out.println("Lazy?: " + constraint + "; " + constraint.isLazy(context));
			if (!constraint.isLazy(context)) {
//				System.out.println("Adding constraint jobs: " + constraint);
				constraintCount++;
				final ConstraintContext constraintContext = constraint.getConstraintContext();
				Collection allOfKind = constraintContext.getAllOfSourceKind(context);
				for (final Object object : allOfKind) {
//					System.out.println("Adding job: " + constraint + " " + constraintCount + " " + object);
					contextJobExecutor.execute(new Runnable() {
						@Override
						public void run() {
							try {
//								System.out.println("Thread " + Thread.currentThread().getName() + ": running " + constraint + "; " + object);
								if (constraintContext.appliesTo(object, context))
									if (constraint.appliesTo(object, context))
										constraint.check(object, context);
							} catch (EolRuntimeException e) {
								contextStatus.addException(Thread.currentThread(), e);
							}
						}
					}, constraintCount);
				}
			}
		}
		
		if (!waitForJobsSuccessful(contextStatus, contextJobExecutor))
			throw new EolRuntimeException("Error in job thread - aborting");

		((IEvlContextParallel) context).endParallel();
		
		if (fixer != null) {
			fixer.fix(this);
		}
		
		execute(getPost(), context);
		
		return null;
	}

	protected void precacheModel(IModel model, String[] metamodels) throws EolModelLoadingException,
			EolModelElementTypeNotFoundException {
		
		for (int i = 0; i < metamodels.length; i++) {
//			System.out.println(metamodels[i]);
			EmfModel metamodel = new EmfModel();
			StringProperties properties = new StringProperties();
			properties.put(EmfModel.PROPERTY_NAME, "Metamodel");
			properties.put(EmfModel.PROPERTY_MODEL_URI, new File(metamodels[i]));
			properties.put(EmfModel.PROPERTY_READONLOAD, true + "");
			properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
					true + "");
			properties.put(EmfModel.PROPERTY_CACHED, true + "");
			
			metamodel.load(properties, (IRelativePathResolver) null);
			
			Collection<EObject> eclasses = metamodel.getAllOfType("EClass");
			
			for (EObject type : eclasses) {
				Object typeName = type.eGet(type.eClass().getEStructuralFeature("name"));
				if (typeName instanceof String) {
					model.getAllOfType((String) typeName);
					model.getAllOfKind((String) typeName);
				}
			}
		}
	}

	protected boolean waitForJobsSuccessful(final EvlModuleParallelStatus status,
			final ExecutorService jobExecutor) {
		jobExecutor.shutdown();
		
		Thread termWait = new Thread(new Runnable() {
			
			@Override
			public void run() {
				jobExecutor.shutdown();
				
				try {
					jobExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
				} catch (InterruptedException e) {
					status.setWaitException();
				}
				
				status.setComplete();
			}
		});
		termWait.setName("AwaitTermination");
		termWait.start();
		
		try {
			status.stop();
		} catch (InterruptedException e1) {
			System.err.println("InterruptedException on main thread wait");
			e1.printStackTrace();
		}
		
		if (status.getExceptionCount() > 0) {
			jobExecutor.shutdownNow();
			System.err.println("Encountered exceptions:");
			
			//After calling shutdownNow() above, interrupted exceptions can be added
			//to collection iterated here (see EvlModuleParallelStatus) - have amended
			//there to a thread safe collection to avoid ConcurrentModificationExceptions
			for (Throwable t : status.getExceptions())
				System.err.println(t.getMessage());
			return false;
		}
		
		if (status.getWaitException()) {
			System.err.println("InterruptedException while waiting for jobs to complete");
			return false;
		}
		
		return true;
	}
	
	protected IEvlContext[] createThreadContexts(IEvlContext context, int number) {
		IEvlContext[] contexts = new IEvlContext[number];
		
		for (int i = 0; i < number; i++) {
			EvlContext newContext = new EvlContext();
			newContext.setModelRepository(context.getModelRepository());
			prepareContext(newContext);
			context.getFrameStack().put(Variable.createReadOnlyVariable("constraintTrace", context.getConstraintTrace()));
			context.getFrameStack().put(Variable.createReadOnlyVariable("thisModule", this));
			contexts[i] = newContext;
		}
		
		return contexts;
	}
	
	@Override
	public void reset() {
		super.reset();
		context = new EvlContextParallel();
	}	
	
	@Override
	public AST adapt(AST cst, AST parentAst) {
		switch (cst.getType()) {
			case EvlParser.CONSTRAINT: return new ConstraintParallelGraph();
			case EvlParser.CRITIQUE: return new ConstraintParallelGraph();
		}
		return super.adapt(cst, parentAst);
	}
}