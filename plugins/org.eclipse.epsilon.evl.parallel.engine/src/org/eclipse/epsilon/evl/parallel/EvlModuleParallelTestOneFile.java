package org.eclipse.epsilon.evl.parallel;

import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.evl.EvlModule;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;

public class EvlModuleParallelTestOneFile {
	
	static Logger logger;
	static FileHandler handler;
	static final String logPath = "D:\\Google Drive\\Project\\testing\\parallel\\test logs\\";
	static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HHmmss");
	
	static final String nl = System.getProperty("line.separator");
	
	static final File javaEcore = new File("D:\\Google Drive\\Project\\testing\\parallel\\files\\java.ecore");
	static final File javaEvl = new File("D:\\Google Drive\\Project\\testing\\parallel\\files\\LargeTest2.evl");
	static final File modelsDir = new File("D:\\Google Drive\\Project\\testing\\parallel\\test models\\");
	static final String resultsDir = "D:\\Google Drive\\Project\\testing\\parallel\\test results\\";
	static final int[] threads = new int[] { 1 , 2, 3, 4, 5, 6, 7, 8 };
	
	static final int runs = 2;
	static final int versionsToRun = 2;
	
	static EmfModel prepModel(String modelPath) throws Exception {
		
		EmfModel emfModel = new EmfModel();
		StringProperties properties = new StringProperties();
		properties.put(EmfModel.PROPERTY_NAME, "Model");
		properties.put(EmfModel.PROPERTY_FILE_BASED_METAMODEL_URI,
				javaEcore.toURI().toString());
		properties.put(EmfModel.PROPERTY_MODEL_URI, 
				new File(modelPath).toURI().toString());
		properties.put(EmfModel.PROPERTY_READONLOAD, true + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
				true + "");
		properties.put(EmfModel.PROPERTY_CACHED, true + "");
		emfModel.load(properties, (IRelativePathResolver) null);
		
		return emfModel;
	}
	
	static EvlModule prepModule(EmfModel model) throws Exception {
		
		EvlModule module = new EvlModule();
		module.parse(javaEvl);
		module.getContext().getModelRepository().addModel(model);
		return module;
	}
	
	static EvlModule prepParallelModule(EmfModel model, int version) throws Exception {
		EvlModule module = null;
		switch (version) {
			case 0:	module = new EvlModuleParallel(); break;
			case 1:	module = new EvlModuleParallelStatic();	break;
			case 2:	module = new EvlModuleParallelLinear();	break;
			case 3:	module = new EvlModuleParallelLinearStatic(); break;
		}
		
		module.parse(javaEvl);
		module.getContext().getModelRepository().addModel(model);
		return module;
	}
	
	public static void main(String[] args) throws Exception {
		
		String date = dateFormat.format(new Date());
		String logFile = logPath + "TestLog" + date + ".txt";
		new File(logFile).createNewFile();
		
		logger = Logger.getLogger("EvlModuleTestLog");
		logger.setUseParentHandlers(false);
		handler = new FileHandler(logFile);
		logger.addHandler(handler);
		handler.setFormatter(new Formatter() {
			@Override
			public String format(LogRecord record) {
				return record.getMessage() + nl;
			}
		});
		
		long start = 0;
		long end = 0;
		
//		HashMap<String, HashMap<String, Integer>> singleThreadResults = new HashMap<String, HashMap<String,Integer>>(); 
//		HashMap<String, HashMap<String, Integer>> multiThreadResults = new HashMap<String, HashMap<String,Integer>>();
		
		HashMap<String, Integer> singleThreadResult = new HashMap<String, Integer>();
		HashMap<String, Integer> multiThreadResult = new HashMap<String, Integer>();
		
		File[] models = modelsDir.listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".xmi") || name.endsWith(".model");
			}
		});
		
		String resultsFile = resultsDir + date + " results.txt";
		new File(resultsFile).createNewFile();
		PrintWriter ro = new PrintWriter(resultsFile);
		ro.append("Model,Run,Threads,Module,Time(ms),Correct");
		
		for (File model : models) {
			
			System.out.println("Model: " + model.getName());
			logger.info("Model: " + model.getName());
			
			for (int run = 1; run <= runs; run++) {
				
				  ///////////////////////
				 // Run single thread //
				///////////////////////
				System.out.print("Run " + run + ": Start single thread");
				
				EmfModel emfModel = prepModel(model.getPath());
				EvlModule module = prepModule(emfModel);
				
				start = System.currentTimeMillis();
				module.execute();
				end = System.currentTimeMillis();
			
				ro.append(nl + model.getName() + "," + run + ",0,EvlModule," + (end-start));
				
				if (run == 1) {
					singleThreadResult.clear();
					
					for (UnsatisfiedConstraint uc : module.getContext().getUnsatisfiedConstraints()) {
						String message = uc.getMessage();
						Integer currentCount = singleThreadResult.get(message);
						if (currentCount == null)
							singleThreadResult.put(message, 1);
						else
							singleThreadResult.put(message, currentCount + 1);
					}
				}

				emfModel.disposeModel();
				System.out.println(" - done");
				logger.info("Run " + run + ": Single thread - done");
				
				  //////////////////////
				 // Run multi-thread //
				//////////////////////
				System.out.println("Run " + run + ": Start multi thread");
				logger.info("Run " + run + ": Start multi thread");
				for (int numThreads : threads) {
					System.out.println("Run " + run + ": " + numThreads + " threads");
					logger.info("Run " + run + ": " + numThreads + " threads");
					for (int version = 0; version < versionsToRun; version++) {
						
						String versionString = "";
						switch (version) {
							case 0:	versionString = "EvlModuleParallel"; break;
							case 1:	versionString = "EvlModuleParallelStatic"; break;
							case 2:	versionString = "EvlModuleParallelLinear"; break;
							case 3:	versionString = "EvlModuleParallelLinearStatic"; break;
						}
						
						System.out.println("Run " + run + ": module " + versionString);
						logger.info("Run " + run + ": module " + versionString);
						
						emfModel = prepModel(model.getPath());
						module = prepParallelModule(emfModel, version);
							
						if (version == 1)
							((EvlModuleParallelStatic) module).setNumThreads(numThreads);
						else
							((EvlModuleParallel) module).setNumThreads(numThreads);
						
						start = System.currentTimeMillis();
						module.execute();
						end = System.currentTimeMillis();
						
						multiThreadResult.clear();
						
						Iterator<UnsatisfiedConstraint> ucs = module.getContext().getUnsatisfiedConstraints().iterator();
						while (ucs.hasNext()) {
							String message = ucs.next().getMessage();
							Integer currentCount = multiThreadResult.get(message);
							if (currentCount == null)
								multiThreadResult.put(message, 1);
							else
								multiThreadResult.put(message, currentCount + 1);
						}
							
						String append;
						
						if (checkResult(singleThreadResult, multiThreadResult)) {
							append = "Yes";
							System.out.println("Result - ok");
							logger.info("Result - ok");
						}
						else {
							append = "No";
							System.out.println("Result - error");
							logger.info("Result - error");
						}
						
						ro.append(nl + model.getName() + "," + run + "," + numThreads + "," + versionString +"," + (end-start) + "," + append);
							
						emfModel.disposeModel();
					}
				}
			}
		}
		
		ro.close();
		System.out.println("Run complete");
		logger.info("Run complete");
		
		
		handler.close();
		logger.removeHandler(handler);
	}

	static boolean checkResult(HashMap<String, Integer> singleThreadResult,
			HashMap<String, Integer> multiThreadResult) {
		
//		System.out.println("\nIn Check Result");
//		System.out.println("single: " + singleThreadResult.size());
//		System.out.println("multi: " + multiThreadResult.size());
		
		if (singleThreadResult.size() != multiThreadResult.size())
			return false;
		
		String[] results = new String[singleThreadResult.size()];
		singleThreadResult.keySet().toArray(results);
		Arrays.sort(results);
		
		for (int i = 0; i < results.length; i++) {
			
//			System.out.println("\nResult: " + results[i]);
//			System.out.println("Single: " + singleThreadResult.get(results[i]));
//			System.out.println("Multi: " + multiThreadResult.get(results[i]));
					
			if (!singleThreadResult.get(results[i]).equals(multiThreadResult.get(results[i]))) {
				return false;
			}
		}
		return true;
	}
}
