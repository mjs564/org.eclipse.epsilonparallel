package org.eclipse.epsilon.evl.parallel.execute;

import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class EvlModuleParallelStatus {
	
	//ConcurrentHashMap used here in case interrupted exceptions are added
	//to this collection after calling threadpoolexecutor.shutdownnow()
	//in EvlModuleParallelGraph.waitForJobsSuccessful(). Such additions
	//can trigger ConcurrenModificationException when iterating this
	//collection if not thread safe.
	private ConcurrentHashMap<String, Throwable> exceptions = new ConcurrentHashMap<String, Throwable>();
	
	private boolean complete = false;
	private boolean waitException = false;
	private boolean stop = false;
	
	public synchronized boolean getComplete() {
		return complete;
	}
	
	public synchronized void setComplete() {
		complete = true;
		stop = true;
		notify();
	}
	
	public synchronized boolean getWaitException() {
		return waitException;
	}
	
	public synchronized void setWaitException() {
		waitException = true;
		stop = true;
		notify();
	}
	
	public synchronized void addException(Thread t, Throwable e) {
		exceptions.put(t.getName(), e);
		stop = true;
		notify();
	}
	
	public synchronized int getExceptionCount() {
		return exceptions.size();
	}
	
	public synchronized Collection<Throwable> getExceptions() {
		return exceptions.values();
	}
	
	public synchronized void stop() throws InterruptedException {
		while (!stop) wait();
	}
	
	public synchronized void reset() {
		exceptions = new ConcurrentHashMap<String, Throwable>();
		complete = false;
		waitException = false;
		stop = false;
	}
}
