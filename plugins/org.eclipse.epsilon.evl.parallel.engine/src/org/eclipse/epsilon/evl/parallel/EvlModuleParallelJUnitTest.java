package org.eclipse.epsilon.evl.parallel;

import static org.junit.Assert.*;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.evl.EvlModule;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class EvlModuleParallelJUnitTest {

	private String modelPath;
	private String evlPath;
	private String ecorePath;

	private HashMap<String, Integer> singleThreadResult;
	private HashMap<String, Integer> multiThreadResult;
	
	public EvlModuleParallelJUnitTest(String modelPath,
			String evlPath, String ecorePath) {
		this.modelPath = modelPath;
		this.evlPath = evlPath;
		this.ecorePath = ecorePath;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> paths() {
		return Arrays.asList(new Object[][] {
//				{ "D:\\Google Drive\\Project\\testing\\parallel\\test models\\medExample_java.xmi", 
//					"D:\\Google Drive\\Project\\testing\\parallel\\files\\LargeTest1.evl", 
//					"D:\\Google Drive\\Project\\testing\\parallel\\files\\java.ecore" },
//				{ "D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\example.model", 
//					"D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\Y3586707.evl", 
//					"D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\Y3586707.ecore" },
//				{ "D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\example.model", 
//					"D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\depevl.evl", 
//					"D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\Y3586707.ecore" },
//				{ "D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\medExample_java.xmi", 
//					"D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\TestingDependencies.evl", 
//					"D:\\Google Drive\\Project\\testing\\parallel\\files\\java.ecore" },
				{ "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\case1.model", 
					"D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\garage.evl", 
					"D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\garage.ecore" },
				
				{ "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\case2.model", 
					"D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\garage.evl", 
					"D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\garage.ecore" },
					
				{ "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\case3.model", 
					"D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\garage.evl", 
					"D:\\Google Drive\\Project\\examples\\epsilon workspace new\\testcases\\garage.ecore" },
				
				{ "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\examples\\railway-1.xmi", 
					"D:\\Google Drive\\Project\\examples\\epsilon workspace new\\examples\\railway_deps.evl", 
					"D:\\Google Drive\\Project\\examples\\epsilon workspace new\\examples\\railway.ecore" }
				
		});
	}
	
	@Test
	public void testEquivalence() throws Exception {
		for (int i = 0; i < 100; i++) {
			initialise();
			assertTrue(checkResult());
		}
	}
	
	private boolean checkResult() {
		System.out.println("Single: " + singleThreadResult.size());
		for (String s : singleThreadResult.keySet()) {
			System.out.println("Single: " + s);
			System.out.println("Single: " + singleThreadResult.get(s));
		}
		System.out.println("Multi: " + multiThreadResult.size());
		for (String s : multiThreadResult.keySet()) {
			System.out.println("Multi: " + s);
			System.out.println("Multi: " + multiThreadResult.get(s));
		}
		
		if (singleThreadResult.size() != multiThreadResult.size())
			return false;
		
		String[] results = new String[singleThreadResult.size()];
		singleThreadResult.keySet().toArray(results);
		Arrays.sort(results);
		
		for (int i = 0; i < results.length; i++) {
			if (!singleThreadResult.get(results[i]).equals(multiThreadResult.get(results[i]))) {
				System.out.println(results[i]);
				System.out.println(singleThreadResult.get(results[i]));
				System.out.println(multiThreadResult.get(results[i]));
				return false;
			}
		}
		return true;
	}
	
	public void initialise() throws Exception {
		EmfModel modelOriginal = loadModel();
		EvlModule moduleOriginal = new EvlModule();
		prepModule(modelOriginal, moduleOriginal, true);
		
		EmfModel modelParallel = loadModel();
		EvlModuleParallelGraph moduleParallel = new EvlModuleParallelGraph();
		moduleParallel.setNumThreads(4);
		prepModule(modelParallel, moduleParallel, false);
	}

	private void prepModule(EmfModel model, EvlModule module, boolean singleThread) throws Exception,
			EolRuntimeException {
		module.parse(new File(evlPath));
		module.getContext().getModelRepository().addModel(model);
		module.execute();
		if (singleThread)
			singleThreadResult = new HashMap<String, Integer>();
		else
			multiThreadResult = new HashMap<String, Integer>();
			
		for (UnsatisfiedConstraint uc : module.getContext().getUnsatisfiedConstraints()) {
			String message = uc.getMessage();
			Integer currentCount = singleThread ? singleThreadResult.get(message) : multiThreadResult.get(message);
			if (currentCount == null) {
				if (singleThread)
					singleThreadResult.put(message, 1);
				else
					multiThreadResult.put(message, 1);
			}
			else {
				if (singleThread)
					singleThreadResult.put(message, currentCount + 1);
				else
					multiThreadResult.put(message, currentCount + 1);
			}
		}
		model.disposeModel();
	}
	
	private EmfModel loadModel() throws EolModelLoadingException {
		EmfModel model = new EmfModel();
		StringProperties properties = new StringProperties();
		properties.put(EmfModel.PROPERTY_NAME, "Model");
		properties.put(EmfModel.PROPERTY_FILE_BASED_METAMODEL_URI,
				new File(ecorePath).toURI().toString());
		properties.put(EmfModel.PROPERTY_MODEL_URI, 
				new File(modelPath).toURI().toString());
		properties.put(EmfModel.PROPERTY_READONLOAD, true + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
				true + "");
		properties.put(EmfModel.PROPERTY_CACHED, true + "");
		
		model.load(properties, (IRelativePathResolver) null);
		
		return model;
	}
}
