package org.eclipse.epsilon.evl.parallel.trace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.trace.ConstraintTrace;
import org.eclipse.epsilon.evl.trace.ConstraintTraceItem;

public class ConstraintTraceParallel extends ConstraintTrace {
	
	  ///////////////////////////////
	 // ConcurrentHashMap version //
	///////////////////////////////
//	protected ConcurrentHashMap<Object, ConcurrentHashMap<Constraint, Boolean>> storageParallel = 
//			new ConcurrentHashMap<Object, ConcurrentHashMap<Constraint,Boolean>>();
//	@Override
//	public boolean isChecked(Constraint constraint, Object object)
//			throws EolRuntimeException {
//		ConcurrentHashMap<Constraint, Boolean> results = storageParallel.get(object);
//		if (results == null){
//			return false;
//		}
//		else {
//			return results.get(constraint) != null;
//		}
//	}
//	
//	@Override
//	public void addChecked(Constraint constraint, Object object, boolean result) {
//		ConcurrentHashMap<Constraint, Boolean> results = storageParallel.get(object);
//		if (results == null){
//			results = new ConcurrentHashMap<Constraint, Boolean>();
//			storageParallel.put(object, results);
//		}
//		results.put(constraint, result);
//	}
//	
//	@Override
//	public boolean isSatisfied(Constraint constraint, Object object) {
//		ConcurrentHashMap<Constraint, Boolean> results = storageParallel.get(object);
//		if (results == null)
//			return false;
//		else
//			return results.get(constraint).booleanValue();
//	}
//	
//	public void setMaxThreads(int numThreads) {
//	}
	
	
	  ////////////////////////
	 // Do nothing version //
	////////////////////////
//	@Override
//	public boolean isChecked(Constraint constraint, Object object)
//			throws EolRuntimeException {
//		return false;
//	}
//	
//	@Override
//	public void addChecked(Constraint constraint, Object object, boolean result) {
//	}
//
//	public void setMaxThreads(int numThreads) {
//	}
	
	
	  //////////////////////////
	 // Synchronised version //
	//////////////////////////
	protected HashMap<Object, HashMap<Constraint, Boolean>> storage = new HashMap<Object, HashMap<Constraint, Boolean>>();
	protected ArrayList<ConstraintTraceItem> iterable = new ArrayList<ConstraintTraceItem>();
	
	protected HashMap<Object, HashSet<Constraint>> constraintChecks = new HashMap<Object, HashSet<Constraint>>();
	protected Object lock = new Object();
	protected int threadsWaitingCount = 0;
	protected int maxThreadsWaiting = 0;

	public void setMaxThreads(int numThreads) {
		maxThreadsWaiting = numThreads;
	}
	
	@Override
	public boolean isChecked(Constraint constraint, Object object) throws EolRuntimeException {
		synchronized(constraintChecks) {
//			System.out.println(Thread.currentThread().getName() + " checking cache: " + constraint + "; object: " + object);
			HashSet<Constraint> constraints = constraintChecks.get(object); 
			if (constraints == null) {
//				System.out.println(Thread.currentThread().getName() + " adding to cache: " + constraint + "; object: " + object);
				constraints = new HashSet<Constraint>();
				constraints.add(constraint);
				constraintChecks.put(object, constraints);
				return false;
			}
			else if (!constraints.contains(constraint)) {
//				System.out.println(Thread.currentThread().getName() + " adding to cache: " + constraint + "; object: " + object);
				constraints.add(constraint);
				return false;
			}
		}
//		System.out.println(Thread.currentThread().getName() + " found in cache: " + constraint + "; object: " + object); 
		synchronized (lock) {
			while (storage.get(object) == null || storage.get(object).get(constraint) == null) {
//				System.out.println(Thread.currentThread().getName() + " waiting");
				if (++threadsWaitingCount < maxThreadsWaiting) {
//					System.out.println(Thread.currentThread().getName() + " BW - Max threads waiting = " + maxThreadsWaiting);
//					System.out.println(Thread.currentThread().getName() + " BW - Num threads waiting = " + threadsWaitingCount);
					try {
						lock.wait();
					}
					catch (InterruptedException e) {
						throw new EolRuntimeException("Thread interrupted on wait");
					}
					--threadsWaitingCount;
//					System.out.println(Thread.currentThread().getName() + " AW - Max threads waiting = " + maxThreadsWaiting);
//					System.out.println(Thread.currentThread().getName() + " AW - Num threads waiting = " + threadsWaitingCount);
				}
				else {
					throw new EolRuntimeException("Deadlock - cyclic constraint dependencies");
				}
//				System.out.println(Thread.currentThread().getName() + " finished waiting");
			}
		}
		return true;
	}
	
	@Override
	public boolean isSatisfied(Constraint constraint, Object object) {
//		System.out.println(Thread.currentThread().getName() + " getting result: " + constraint + " " + object);
		HashMap<Constraint, Boolean> results;
		
		synchronized (lock) {
			results = storage.get(object);
		}
		
		if (results == null){
			return false;
		}
		else {
			synchronized (lock) {
				return results.get(constraint).booleanValue();
			}
		}
	}
	
	@Override
	public void addChecked(Constraint constraint, Object object, boolean result){
		synchronized (lock) {
//			System.out.println(Thread.currentThread().getName() + " adding checked constraint: " + constraint + "; object: " + object);
			HashMap<Constraint, Boolean> constraintResults = storage.get(object);
			if (constraintResults == null) {
				constraintResults = new HashMap<Constraint, Boolean>();
				storage.put(object, constraintResults);
			}
			constraintResults.put(constraint, result);
			iterable.add(new ConstraintTraceItem(object, constraint, result));
//			System.out.println(Thread.currentThread().getName() + " completed constraint: " + constraint + "; object: " + object);
			lock.notifyAll();
		}
	}

}
