package org.eclipse.epsilon.evl.parallel.execute.context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.epsilon.eol.execute.context.FrameStack;
import org.eclipse.epsilon.eol.execute.operations.contributors.OperationContributorRegistry;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;
import org.eclipse.epsilon.evl.execute.context.EvlContext;
import org.eclipse.epsilon.evl.parallel.trace.ConstraintTraceParallel;
import org.eclipse.epsilon.evl.trace.ConstraintTrace;

public class EvlContextParallel extends EvlContext implements IEvlContextParallel {
	
//	public Object lock = new Object();
	protected boolean parallel = false;
	protected Map<String, FrameStack> concurrentFrameStacks;
	protected Map<String, OperationContributorRegistry> methodContributorRegistryParallel;
	protected Map<String, ArrayList<UnsatisfiedConstraint>> unsatisfiedConstraintsParallel;
	
	protected ConstraintTraceParallel constraintTraceParallel = new ConstraintTraceParallel();
	
	protected Set<Constraint> constraintsDependedOn;
	
	@Override
	public FrameStack getFrameStack() {
		if (!parallel)
			return frameStack;
		else if (concurrentFrameStacks != null)
			return concurrentFrameStacks.get(Thread.currentThread().getName());
		else
			return null;
	}
	
	@Override
	public OperationContributorRegistry getOperationContributorRegistry() {
		if (!parallel)
			return methodContributorRegistry;
		else if (methodContributorRegistry != null)
			return methodContributorRegistryParallel.get(Thread.currentThread().getName());
		else
			return null;
	}
	
	@Override
	public ArrayList<UnsatisfiedConstraint> getUnsatisfiedConstraints() {
		if (!parallel)
			return unsatisfiedConstraints;
		else if (unsatisfiedConstraintsParallel != null)
			return unsatisfiedConstraintsParallel.get(Thread.currentThread().getName());
		else
			return null;
	}

	@Override
	public void goParallel() {
		concurrentFrameStacks = new ConcurrentHashMap<String, FrameStack>();
		methodContributorRegistryParallel = new ConcurrentHashMap<String, OperationContributorRegistry>();
		unsatisfiedConstraintsParallel = new ConcurrentHashMap<String, ArrayList<UnsatisfiedConstraint>>();
		parallel = true;
	}

	@Override
	public void endParallel() {
		parallel = false;
		concurrentFrameStacks = null;
		methodContributorRegistryParallel = null;
		
		  ////////////////////////////////////
		 // Dedupe unsatisfied constraints //
		////////////////////////////////////
//		Set<UnsatisfiedConstraint> unsatisfiedConstraintsSet = new HashSet<UnsatisfiedConstraint>();
//		for (ArrayList<UnsatisfiedConstraint> uc : unsatisfiedConstraintsParallel.values())
//			unsatisfiedConstraintsSet.addAll(uc);
//		unsatisfiedConstraints = new ArrayList<UnsatisfiedConstraint>(unsatisfiedConstraintsSet);
//		unsatisfiedConstraintsParallel = null;
		
		  //////////////////////////////////////////
		 // Don't dedupe unsatisfied constraints //
		//////////////////////////////////////////
		unsatisfiedConstraints = new ArrayList<UnsatisfiedConstraint>();
		for (ArrayList<UnsatisfiedConstraint> uc : unsatisfiedConstraintsParallel.values())
			unsatisfiedConstraints.addAll(uc);
		unsatisfiedConstraintsParallel = null;
	}

	@Override
	public void addParallelFrameStack() {
		concurrentFrameStacks.put(Thread.currentThread().getName(), frameStack.clone());
	}
	
	@Override
	public void addParallelOperationContributorRegistry() {
		methodContributorRegistryParallel.put(Thread.currentThread().getName(), new OperationContributorRegistry());
	}
	
	@Override
	public void addParallelUnsatisfiedConstraints() {
		unsatisfiedConstraintsParallel.put(Thread.currentThread().getName(), new ArrayList<UnsatisfiedConstraint>());
	}
	
	@Override
	public ConstraintTrace getConstraintTrace() {
		return constraintTraceParallel;
	}

	@Override
	public void setConstraintsDependedOn(Set<Constraint> constraints) {
		constraintsDependedOn = constraints;
	}

	@Override
	public Set<Constraint> getConstraintsDependedOn() {
		return constraintsDependedOn;
	}
}