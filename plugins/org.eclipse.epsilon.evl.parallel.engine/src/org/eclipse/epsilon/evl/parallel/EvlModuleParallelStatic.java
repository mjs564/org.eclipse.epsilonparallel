/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Dimitrios Kolovos - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.evl.parallel;

import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.execute.context.Variable;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.evl.EvlModule;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.dom.ConstraintContext;
import org.eclipse.epsilon.evl.execute.EvlOperationFactory;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;
import org.eclipse.epsilon.evl.execute.context.EvlContext;
import org.eclipse.epsilon.evl.execute.context.IEvlContext;
import org.eclipse.epsilon.evl.parallel.execute.EvlModuleParallelStatus;
import org.eclipse.epsilon.evl.parallel.execute.context.EvlContextParallel;
import org.eclipse.epsilon.evl.parallel.execute.context.IEvlContextParallel;
import org.eclipse.epsilon.evl.parallel.trace.ConstraintTraceParallel;

public class EvlModuleParallelStatic extends EvlModule {
	
	public static void main(String[] args) throws Exception {
		EmfModel emfModel = new EmfModel();
		StringProperties properties = new StringProperties();
		properties.put(EmfModel.PROPERTY_NAME, "Model");
		properties.put(EmfModel.PROPERTY_FILE_BASED_METAMODEL_URI,
				new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\Y3586707.ecore").toURI().toString());
		properties.put(EmfModel.PROPERTY_MODEL_URI, 
				new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\example.model").toURI().toString());
		properties.put(EmfModel.PROPERTY_READONLOAD, true + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
				true + "");
		properties.put(EmfModel.PROPERTY_CACHED, true + "");
		
		///Users/matt/Google Drive/Project/modisco/workspace/EpsilonSource/java.ecore
		
		
		
		int mb = 1024 * 1024;
		
		Runtime r = Runtime.getRuntime();
		 
		System.out.println("Total memory: " + (r.totalMemory() / mb));
		System.out.println("Free memory: " + (r.freeMemory() / mb));
		System.out.println("Max memory: " + (r.maxMemory() / mb));
		
		
			
		emfModel.load(properties, (IRelativePathResolver) null);
		
		EvlModuleParallel module = new EvlModuleParallel();
		module.setNumThreads(1);
		module.parse(new File("D:\\Google Drive\\Project\\examples\\epsilon workspace\\examples\\Y3586707.evl"));
		
		module.getContext().getModelRepository().addModel(emfModel);
		
		module.execute();
		
		//post process
		Collection<UnsatisfiedConstraint> unsatisfied = module.getContext().getUnsatisfiedConstraints();
		
		if (unsatisfied.size() > 0) {
			System.err.println(unsatisfied.size() + " constraint(s) have not been satisfied");
			for (UnsatisfiedConstraint uc : unsatisfied) {
				System.err.println(uc.getMessage());
			}
		}
		else {
			System.out.println("All constraints have been satisfied");
		}
	}
	
	protected int numThreads = 1;
	
	protected volatile ArrayList<Object[]> contextJobs = new ArrayList<Object[]>();
	protected volatile ArrayList<Object[]> constraintJobs = new ArrayList<Object[]>();
	
	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
		((ConstraintTraceParallel) context.getConstraintTrace()).setMaxThreads(numThreads);
	}
	
	public EvlModuleParallelStatic() {
		reset();
	}
	
	@Override
	public Object execute() throws EolRuntimeException {
		// Initialise the context
		prepareContext(context);
		context.setOperationFactory(new EvlOperationFactory());
		context.getFrameStack().put(Variable.createReadOnlyVariable("constraintTrace", context.getConstraintTrace()));
		context.getFrameStack().put(Variable.createReadOnlyVariable("thisModule", this));
		
		execute(getPre(), context);
		
//		System.out.println("# Threads: " + numThreads);
		final EvlModuleParallelStatus contextStatus = new EvlModuleParallelStatus();		
		final EvlModuleParallelThreadFactory factory = new EvlModuleParallelThreadFactory((IEvlContextParallel) context, null);
		((IEvlContextParallel) context).goParallel();
		
		//Process context-level guard block
		final ExecutorService contextJobExecutor = new EvlThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new LinkedBlockingQueue<Runnable>(), factory, contextStatus); 
		
		for (final ConstraintContext constraintContext : getConstraintContexts()) {
//			System.out.println("Context: " + constraintContext);
			Collection allOfKind = constraintContext.getAllOfSourceKind(context);
//			System.out.println(allOfKind.size());
			
			for (final Object object : allOfKind) {
//				System.out.println("Adding job " + ++count);
				contextJobExecutor.execute(getContextRunnable(constraintContext, object, contextStatus));
			}
		}
		
		if (!waitForJobsSuccessful(contextStatus, contextJobExecutor))
			throw new EolRuntimeException("Error in job thread - aborting");

//		System.out.println("contextJobExecutor: " + ((EvlThreadPoolExecutor) contextJobExecutor).getActiveCount());
//		System.out.println("\n" + contextJobs.size());
		
		
		//Process constraint-level guard block
		final EvlModuleParallelStatus constraintStatus = new EvlModuleParallelStatus();
		final EvlPriorityThreadPoolExecutor constraintJobExecutor = new EvlPriorityThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new PriorityBlockingQueue<Runnable>(), factory, contextStatus);
//		int constraintCount = 0;
//		int contextJobCount = 0;
//		
//		final MyCounter mc2 = new MyCounter();
		
		ArrayList<Object[]> lowPriorityConstraintJobs = new ArrayList<Object[]>();
		HashMap<Constraint, Boolean> constraintsUseSatisfies = new HashMap<Constraint, Boolean>();
		
		for (final Object[] job : contextJobs) {
//			System.out.println("For contextJob " + ++contextJobCount);
			for (final Constraint constraint : ((ConstraintContext) job[0]).getConstraints()) {
//				System.out.println("Adding constraintJob runnable " + ++constraintCount);
//				System.out.println("Adding constraint: " + constraint);
				Boolean usesSatisfies = constraintsUseSatisfies.get(constraint);
				if (usesSatisfies == null) {
					usesSatisfies = constraint.guardBlockUsesSatisfies();
					constraintsUseSatisfies.put(constraint, usesSatisfies);
				}
				if (!usesSatisfies)
					constraintJobExecutor.execute(getConstraintGuardRunnable(constraint, job[1], constraintStatus), 1);
				else {
//					System.out.println("Deferring constraint: " + constraint);
					lowPriorityConstraintJobs.add(new Object[] { constraint, job[1] });
				}
			}
		}
		
		for (final Object[] job : lowPriorityConstraintJobs) {
//			System.out.println("Adding deferred constraint: " + job[0]);
			constraintJobExecutor.execute(getConstraintGuardRunnable((Constraint) job[0], job[1], constraintStatus), 2);
		}
		
		if (!waitForJobsSuccessful(constraintStatus, constraintJobExecutor))
			throw new EolRuntimeException("Error in job thread");

//		System.out.println("constraintJobExecutor: " + ((EvlThreadPoolExecutor) constraintJobExecutor).getActiveCount());
//		System.out.println("# ConstraintJobs: " + constraintJobs.size());
		
		//Process constraint check block
		final EvlModuleParallelStatus checkStatus = new EvlModuleParallelStatus();
		final EvlPriorityThreadPoolExecutor checkBlockExecutor = new EvlPriorityThreadPoolExecutor(numThreads, 
				numThreads, 0, TimeUnit.NANOSECONDS, new PriorityBlockingQueue<Runnable>(), factory, contextStatus);
		
		lowPriorityConstraintJobs.clear();
		constraintsUseSatisfies.clear();
		
		
		for (final Object[] job: constraintJobs) {
			Constraint constraint = ((Constraint) job[0]); 
			Boolean usesSatisfies = constraintsUseSatisfies.get(constraint);
			if (usesSatisfies == null) {
//				System.out.println("Adding constraint: " + constraint);
				usesSatisfies = (constraint.checkBlockUsesSatisfies() || constraint.messageBlockUsesSatisfies());
				constraintsUseSatisfies.put(constraint, usesSatisfies);
			}
			if (!usesSatisfies)
				checkBlockExecutor.execute(getConstraintCheckRunnable(job, checkStatus), 1);
			else {
//				System.out.println("Deferring constraint: " + constraint);
				lowPriorityConstraintJobs.add(job);
			}
		}
		
		for (final Object[] job : lowPriorityConstraintJobs) {
//			System.out.println("Adding deferred constraint: " + job[0]);
			constraintJobExecutor.execute(getConstraintCheckRunnable(job, constraintStatus), 2);
		}
		
		if (!waitForJobsSuccessful(checkStatus, checkBlockExecutor))
			throw new EolRuntimeException("Error in job thread");
		
//		System.out.println("checkBlockExecutor: " + ((EvlThreadPoolExecutor) checkBlockExecutor).getActiveCount());
		
		((IEvlContextParallel) context).endParallel();
		
		if (fixer != null) {
			fixer.fix(this);
		}
		
		execute(getPost(), context);
		
		return null;
	}

	protected boolean waitForJobsSuccessful(final EvlModuleParallelStatus status,
			final ExecutorService jobExecutor) {
		jobExecutor.shutdown();
		
		Thread termWait = new Thread(new Runnable() {
			
			@Override
			public void run() {
				jobExecutor.shutdown();
				
				try {
					jobExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
				} catch (InterruptedException e) {
					status.setWaitException();
				}
				
				status.setComplete();
			}
		});
		termWait.setName("AwaitTermination");
		termWait.start();
		
		try {
			status.stop();
		} catch (InterruptedException e1) {
			System.err.println("InterruptedException on main thread wait");
			e1.printStackTrace();
		}
		
		if (status.getExceptionCount() > 0) {
			jobExecutor.shutdownNow();
			System.err.println("Encountered exceptions:");
			for (Throwable t : status.getExceptions())
				System.err.println(t.getMessage());
			return false;
		}
		
		if (status.getWaitException()) {
			System.err.println("InterruptedException while waiting for jobs to complete");
			return false;
		}
		
		return true;
	}
	
	protected IEvlContext[] createThreadContexts(IEvlContext context, int number) {
		IEvlContext[] contexts = new IEvlContext[number];
		
		for (int i = 0; i < number; i++) {
			EvlContext newContext = new EvlContext();
			newContext.setModelRepository(context.getModelRepository());
			prepareContext(newContext);
			context.getFrameStack().put(Variable.createReadOnlyVariable("constraintTrace", context.getConstraintTrace()));
			context.getFrameStack().put(Variable.createReadOnlyVariable("thisModule", this));
			contexts[i] = newContext;
		}
		
		return contexts;
	}
	
	@Override
	public void reset() {
		super.reset();
		context = new EvlContextParallel();
	}
	
	class MyCounter {
		int count = 0;
		
		public int increment() {
			return ++count;
		}
	}
	
	protected Runnable getContextRunnable(final ConstraintContext constraintContext,
			final Object object, final EvlModuleParallelStatus contextStatus) {
		return new Runnable() {
			@Override
			public void run() {
//				System.out.println("Running context: " + constraintContext);
				try {
					if (constraintContext.appliesTo(object, context)) {
//						System.out.println("Adding context job: " + constraintContext + ", " + object);
						synchronized (contextJobs) {
							contextJobs.add(new Object[] { constraintContext, object } );
						}
					}
				} catch (EolRuntimeException e) {
//					System.err.println("Error running thread:\n" + constraintContext + 
//							"\n" + object + "\n" + e.getMessage());
					contextStatus.addException(Thread.currentThread(), e);
				}
			}
		};
	}
	
	protected Runnable getConstraintGuardRunnable(final Constraint constraint,
			final Object object, final EvlModuleParallelStatus constraintStatus) {
		return new Runnable() {
			
			@Override
			public void run() {
//						System.out.println("Running job " + mc2.increment());
				try {
					if (!constraint.isLazy(context) && constraint.appliesTo(object, context)) {
						synchronized(constraintJobs) {
//									System.out.println("Adding constraint job: " + constraint + ", " + job[1]);
							constraintJobs.add(new Object[] { constraint, object } );
						}
					}
				} catch (EolRuntimeException e) {
					System.err.println("Error running thread:\n" + constraint + 
							"\n" + object + "\n" + e.getMessage());
					constraintStatus.addException(Thread.currentThread(), e);
				}
			}
		};
	}
	
	protected Runnable getConstraintCheckRunnable(final Object[] job,
			final EvlModuleParallelStatus checkStatus) {
		return new Runnable() {
			
			@Override
			public void run() {
				try {
					((Constraint) job[0]).check(job[1], context);
				} catch (EolRuntimeException e) {
//					System.err.println("Error running thread:\n" + 
//							object[0] + "\n" + object[1] + "\n" + e.getMessage());
					checkStatus.addException(Thread.currentThread(), e);
				}
			}
		};
	}
}
