package org.eclipse.epsilon.evl.parallel.graph;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.epsilon.evl.EvlModule;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.dom.ConstraintContext;
import org.eclipse.epsilon.evl.execute.exceptions.EvlConstraintNotFoundException;
import org.junit.Test;

public class EvlGraphTest {
	
	EvlGraph graph;
	String[] nums = new String[] { "First", "Second", "Third", "Fourth", "Fifth", "Six", "Seven", "Eight", "Nine", "Ten" };
	
	@Test
	public void testNull() throws EvlConstraintNotFoundException {
		graph = new EvlGraph();
		graph.addConstraintContexts(null);
		
		List<Constraint> sequence = graph.getEvlSequence();
		Set<Constraint> all = graph.getAllConstraintsDependedOn();
		
		assertEquals(0, sequence.size());
		assertEquals(0, all.size());
	}
	
	@Test
	public void testEmpty() throws EvlConstraintNotFoundException {
		graph = new EvlGraph();
		graph.addConstraintContexts(new ArrayList<ConstraintContext>());
		
		List<Constraint> sequence = graph.getEvlSequence();
		Set<Constraint> all = graph.getAllConstraintsDependedOn();
		
		assertEquals(0, sequence.size());
		assertEquals(0, all.size());
	}
	
	@Test(expected=EvlConstraintNotFoundException.class)
	public void testEvlConstraintNotFoundException() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\depNotExist.evl";
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[1]);
		
		runTest(path, 1, dependedOnStrings);
	}

	
	@Test
	public void testNoDependencies() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\noDeps.evl";
		
		runTest(path, 4, new HashSet<String>());
	}
	
	@Test
	public void testConstraintDependency() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\constrDep.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[0]);
		
		runTest(path, 4, dependedOnStrings);
	}
	
	@Test
	public void testConstraintTwoDependencies() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\constr2Deps.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[0]);
		dependedOnStrings.add(nums[1]);
		
		runTest(path, 4, dependedOnStrings);
	}
	
	@Test
	public void testConstraintTwoDependenciesChain() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\constr2DepsChain.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[1]);
		dependedOnStrings.add(nums[2]);
		
		runTest(path, 4, dependedOnStrings);
	}
	
	@Test
	public void testContextDependency() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\contDep.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[0]);
		
		runTest(path, 4, dependedOnStrings);
	}
	
	@Test
	public void testContextConstraintDependencies() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\contconstrDeps.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[0]);
		dependedOnStrings.add(nums[1]);
		dependedOnStrings.add(nums[2]);
		
		runTest(path, 4, dependedOnStrings);
	}
	
	@Test
	public void testConstraintCycle() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\constrCycle.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[0]);
		dependedOnStrings.add(nums[1]);
		
		runTest(path, 2, dependedOnStrings);
	}
	
	@Test
	public void testContextContainsDependency() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\contextContainsDep.evl";
		
		runTest(path, 1, new HashSet<String>());
	}
	
	@Test
	public void testComplexDependencies() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\complexDeps.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[0]);
		dependedOnStrings.add(nums[1]);
		dependedOnStrings.add(nums[2]);
		dependedOnStrings.add(nums[3]);
		
		runTest(path, 5, dependedOnStrings);
	}
	
	@Test
	public void testLazyDependencies() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\lazy.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[0]);
		
		runTest(path, 2, dependedOnStrings);
	}
	
	@Test
	public void testLazyNoDependencies() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\lazyNoDeps.evl";
		
		runTest(path, 2, new HashSet<String>());
	}
	
	@Test
	public void testLazyConjoinedDependencies() throws Exception {
		String path = "D:\\Google Drive\\Project\\examples\\epsilon workspace new\\test\\lazyAnded.evl";
		
		Set<String> dependedOnStrings = new HashSet<String>();
		dependedOnStrings.add(nums[0]);
		dependedOnStrings.add(nums[1]);
		
		runTest(path, 3, dependedOnStrings);
	}
	
	private void runTest(String path, int sequenceLength, Set<String> dependedOnStrings) throws Exception {
		EvlModule module = new EvlModule();
		module.parse(new File(path));
		
		graph = new EvlGraph();
		graph.addConstraintContexts(module.getConstraintContexts());
		
		List<Constraint> sequence = graph.getEvlSequence();
		System.out.println(sequence);
		Set<Constraint> allDependedOn = graph.getAllConstraintsDependedOn();
		System.out.println(allDependedOn);
		int count = 0;
		
		assertEquals(sequenceLength, sequence.size());
		for (Constraint constraint : sequence) {
			assertEquals(nums[count++], constraint.getText());
		}
		
		assertEquals(dependedOnStrings.size(), allDependedOn.size());
		for (Constraint dependedOn : allDependedOn)
			assertTrue(dependedOnStrings.contains(dependedOn.getText()));
	}
}
