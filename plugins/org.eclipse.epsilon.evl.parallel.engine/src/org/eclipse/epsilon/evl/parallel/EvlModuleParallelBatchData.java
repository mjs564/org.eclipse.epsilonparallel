package org.eclipse.epsilon.evl.parallel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class EvlModuleParallelBatchData {
	
	protected ConcurrentHashMap<String, ArrayList<Object[]>> storage;
	
	public EvlModuleParallelBatchData() {
		storage = new ConcurrentHashMap<String, ArrayList<Object[]>>();
	}
	
	public synchronized void registerThread() {
		String name = Thread.currentThread().getName();
		if (!storage.containsKey(name))
			storage.put(name, new ArrayList<Object[]>());
	}

	public synchronized Collection<Object[]> getBatch() {
		ArrayList<Object[]> flattened = new ArrayList<Object[]>();
		for (ArrayList<Object[]> value : storage.values())
			flattened.addAll(value);
		return flattened;
	}
	
	public synchronized void clear() {
		storage = new ConcurrentHashMap<String, ArrayList<Object[]>>();
	}
	
	public void addElement(Object[] value) {
		storage.get(Thread.currentThread().getName()).add(value);
	}
}
