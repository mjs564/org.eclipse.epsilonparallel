//http://stackoverflow.com/questions/12722396/specify-task-order-execution-in-java
package org.eclipse.epsilon.evl.parallel;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.eclipse.epsilon.evl.parallel.execute.EvlModuleParallelStatus;

public class EvlPriorityThreadPoolExecutor extends ThreadPoolExecutor {

	private EvlModuleParallelStatus status;
	
	public EvlPriorityThreadPoolExecutor(int corePoolSize,int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue,
			ThreadFactory threadFactory, EvlModuleParallelStatus status) {
		
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
				threadFactory);
		
		this.status = status;
	}

	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		
		if (t != null)
			status.addException(Thread.currentThread(), t);
	}
	
	public void execute(Runnable command, int priority) {
		execute(new ComparableFutureTask(command, null, priority));
	}
	
	static class ComparableFutureTask<T> extends FutureTask<T> implements Comparable<ComparableFutureTask<T>> {
		
		volatile int priority = 0;
		
		public ComparableFutureTask(Runnable runnable, T result, int priority) {
			super(runnable, result);
			this.priority = priority;
		}

		@Override
		public int compareTo(ComparableFutureTask<T> o) {
			return Integer.valueOf(priority).compareTo(o.priority); 
		}
	}
}
