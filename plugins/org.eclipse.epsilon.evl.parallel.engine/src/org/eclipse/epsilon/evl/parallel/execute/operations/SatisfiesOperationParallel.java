package org.eclipse.epsilon.evl.parallel.execute.operations;

import java.util.List;

import org.eclipse.epsilon.common.parse.AST;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.execute.context.IEolContext;
import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.execute.context.IEvlContext;
import org.eclipse.epsilon.evl.execute.exceptions.EvlConstraintNotFoundException;
import org.eclipse.epsilon.evl.execute.operations.SatisfiesOperation;
import org.eclipse.epsilon.evl.parallel.execute.context.EvlContextParallel;
import org.eclipse.epsilon.evl.parallel.execute.context.IEvlContextParallel;
import org.eclipse.epsilon.evl.parallel.trace.ConstraintTraceParallel;

public class SatisfiesOperationParallel extends SatisfiesOperation {

	public SatisfiesOperationParallel(boolean all) {
		super(all);
	}
	
	@Override
	public Object execute(Object source, List<?> parameters,
			IEolContext context_, AST ast) throws EolRuntimeException {

		if (source == null) return false;
		
		IEvlContextParallel context = (IEvlContextParallel) context_;
		
		for (Object parameter : parameters) {
			String constraintName = context.getPrettyPrinterManager().toString(parameter);
			
			Constraint constraint = context.getModule().getConstraints().getConstraint(constraintName, source, context);
			
//			System.out.println(Thread.currentThread().getName() + " Satisfies: " + constraint + "; " + source);
			
			if (constraint == null) {
				throw new EvlConstraintNotFoundException(constraintName,ast);
			}
			
			boolean valid = constraint.check(source,context);
			
//			if (context.getConstraintTrace().isChecked(constraint, source)) {
//				valid = context.getConstraintTrace().isSatisfied(constraint, source);
//				System.out.println(Thread.currentThread().getName() + " Satisfies isChecked: " + constraint + "; " + source);
//			}
//			else if (constraint.appliesTo(source, context)) {
//				System.out.println(Thread.currentThread().getName() + " Satisfies appliesTo: " + constraint + "; " + source);
//				valid = constraint.check(source, context);
//			}
//			else {
//				valid = false;
//				System.out.println(Thread.currentThread().getName() + " Satisfies !isChecked and !appliesTo: " + constraint + "; " + source);
//				if (context.getConstraintsDependedOn().contains(constraint)) {
//					System.out.println(Thread.currentThread().getName() + " Satisfies addChecked: " + constraint + "; " + source);
//					context.getConstraintTrace().addChecked(constraint, source, valid);
//				}
//			}
				
			
			if (all) {
				if (!valid) return false;
			}
			else {
				if (valid) return true;
			}
		}
		
		return true;
	}
}
