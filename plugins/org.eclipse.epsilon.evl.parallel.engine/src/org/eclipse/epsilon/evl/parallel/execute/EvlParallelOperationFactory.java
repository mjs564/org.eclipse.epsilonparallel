package org.eclipse.epsilon.evl.parallel.execute;

import org.eclipse.epsilon.evl.execute.EvlOperationFactory;
import org.eclipse.epsilon.evl.parallel.execute.operations.SatisfiesOperationParallel;

public class EvlParallelOperationFactory extends EvlOperationFactory {
	public EvlParallelOperationFactory() {
		super();
		operationCache.put("satisfies", new SatisfiesOperationParallel(true));
		operationCache.put("satisfiesOne", new SatisfiesOperationParallel(false));
	}
}
