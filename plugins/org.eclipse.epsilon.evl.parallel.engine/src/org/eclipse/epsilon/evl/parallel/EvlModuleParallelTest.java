package org.eclipse.epsilon.evl.parallel;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.evl.EvlModule;
import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;

public class EvlModuleParallelTest {
	
	static Logger logger;
	static FileHandler handler;
	static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HHmmss");
	
	static final String nl = System.getProperty("line.separator");
	
//	static final String logPath = "D:\\Google Drive\\Project\\testing\\parallel\\test logs\\";
//	static final File javaEcore = new File("D:\\Google Drive\\Project\\testing\\parallel\\files\\java.ecore");
//	static final String modelsDir = "D:\\Google Drive\\Project\\testing\\parallel\\test models\\";
//	static final String filesDir = "D:\\Google Drive\\Project\\testing\\parallel\\files\\";
//	static final String resultsDir = "D:\\Google Drive\\Project\\testing\\parallel\\test results\\";
	
	static final String logPath = "/usr/userfs/m/mjs564/project/testing/logs/";
	//static final File javaEcore = new File("/usr/userfs/m/mjs564/project/testing/files/java.ecore");
	static final String modelsDir = "/usr/userfs/m/mjs564/project/testing/models/";
	static final String filesDir = "/usr/userfs/m/mjs564/project/testing/files/";
	static final String resultsDir = "/usr/userfs/m/mjs564/project/testing/results/";

	static final int[] threads = new int[] { 1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
	
	static int runs;
	static int version;
	
	static PrintWriter ro;
	static HashMap<String, Integer> singleThreadResult = new HashMap<String, Integer>();
	static HashMap<String, Integer> multiThreadResult = new HashMap<String, Integer>();
	
	static EmfModel prepModel(String modelPath, File ecore) throws Exception {
		
		EmfModel emfModel = new EmfModel();
		StringProperties properties = new StringProperties();
		properties.put(EmfModel.PROPERTY_NAME, "Model");
		properties.put(EmfModel.PROPERTY_FILE_BASED_METAMODEL_URI,
				ecore.toURI().toString());
		properties.put(EmfModel.PROPERTY_MODEL_URI, 
				new File(modelPath).toURI().toString());
		properties.put(EmfModel.PROPERTY_READONLOAD, true + "");
		properties.put(EmfModel.PROPERTY_STOREONDISPOSAL, 
				true + "");
		properties.put(EmfModel.PROPERTY_CACHED, true + "");
		emfModel.load(properties, (IRelativePathResolver) null);
		
		return emfModel;
	}
	
	static EvlModule prepModule(EmfModel model, File evl) throws Exception {
		
		EvlModule module = new EvlModule();
		module.parse(evl);
		module.getContext().getModelRepository().addModel(model);
		return module;
	}
	
	static EvlModule prepParallelModule(EmfModel model, File evl, int version) throws Exception {
		EvlModule module = null;
		switch (version) {
			case 0: module = new EvlModule(); break;
			case 1:	module = new EvlModuleParallel(); break;
			case 2:	module = new EvlModuleParallelPreCache();	break;
			case 3:	module = new EvlModuleParallelGraph();	break;
			case 4:	module = new EvlModuleParallelStatic();	break;
			case 5:	module = new EvlModuleParallelLinear();	break;
			case 6:	module = new EvlModuleParallelLinearStatic(); break;
		}
		
		module.parse(evl);
		module.getContext().getModelRepository().addModel(model);
		return module;
	}
	
	static String getVersionString(int version) {
		switch (version) {
			case 0: return "EvlModule";
			case 1:	return "EvlModuleParallel";
			case 2:	return "EvlModuleParallelPreCache";
			case 3:	return "EvlModuleParallelGraph";
			case 4:	return "EvlModuleParallelStatic";
			case 5:	return "EvlModuleParallelLinear";
			case 6:	return "EvlModuleParallelLinearStatic";
		}
		return null;
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length != 6) {
			System.out.println("Usage: EvlModuleParallelTest <version> <#runs> <model> <evl file> <resultfilename> <ecore>");
			System.exit(0);
		}
		
		try {
			version = Integer.parseInt(args[0]);
			runs = Integer.parseInt(args[1]);
		}
		catch (NumberFormatException e) {
			System.err.println("<version> and <#runs> arguments must be valid integers");
			System.exit(0);
		}
		
		File model = new File(modelsDir + args[2]);
		File evl = new File(filesDir + args[3]);
		File ecore = new File(filesDir + args[5]);
		
		String date = dateFormat.format(new Date());
		String fileAppend = date + " " + args[4] + " " + getVersionString(version) + " " + evl.getName() + " " + model.getName() + " ";
		
		String logFile = logPath + fileAppend + "log.txt";
		new File(logFile).createNewFile();
		
		logger = Logger.getLogger("EvlModuleTestLog");
		logger.setUseParentHandlers(false);
		handler = new FileHandler(logFile);
		logger.addHandler(handler);
		handler.setFormatter(new Formatter() {
			@Override
			public String format(LogRecord record) {
				return record.getMessage() + nl;
			}
		});
		
		String resultsFile = resultsDir + fileAppend + "results.csv";
		new File(resultsFile).createNewFile();
		ro = new PrintWriter(resultsFile);
		ro.append("Model,Threads,");
		
		for (int i = 1; i <= runs; i++)
			if (i < runs)
				ro.append(i + ",");
			else
				ro.append(i + "");
		
		System.out.println("Model: " + model.getName());
		System.out.println("EVL: " + args[3]);
		logger.info("Model: " + model.getName());
		logger.info("EVL: " + args[3]);
//		if (version > 0) {
//			EmfModel emfModel = prepModel(model.getPath());
//			EvlModule module = prepModule(emfModel, evl);
//			
//			module.execute();
//			
//			for (UnsatisfiedConstraint uc : module.getContext().getUnsatisfiedConstraints()) {
//				String message = uc.getMessage();
//				Integer currentCount = singleThreadResult.get(message);
//				if (currentCount == null)
//					singleThreadResult.put(message, 1);
//				else
//					singleThreadResult.put(message, currentCount + 1);
//			}
//		}
		
		doRun(model, evl, ecore, 0, 0);
		
		if (version > 0) {
			for (int numThreads : threads) {
				System.out.println("Threads: " + numThreads);
				logger.info("Threads: " + numThreads);
				
				doRun(model, evl, ecore, numThreads, version);
			}
		}
		ro.close();
	
		System.out.println("Run complete");
		logger.info("Run complete");
		System.out.println("Start: " + date);
		logger.info("Start: " + date);
		System.out.println("End: " + dateFormat.format(new Date()));
		logger.info("End: " + dateFormat.format(new Date()));
		
		handler.close();
		logger.removeHandler(handler);
	}

	static void doRun(File model, File evl,	File ecore, int numThreads, int runVersion) throws Exception, EolRuntimeException {
		long start;
		long end;
		for (int run = 1; run <= runs; run++) {
			
			EmfModel emfModel = prepModel(model.getPath(), ecore);
			EvlModule modPar = prepParallelModule(emfModel, evl, runVersion);
			
			if (runVersion == 2)
				((EvlModuleParallelPreCache) modPar).setNumThreads(numThreads);
			else if (runVersion == 3)
				((EvlModuleParallelGraph) modPar).setNumThreads(numThreads);
			else if (runVersion == 4)
				((EvlModuleParallelStatic) modPar).setNumThreads(numThreads);
			else if (runVersion > 0)
				((EvlModuleParallel) modPar).setNumThreads(numThreads);
			
			System.out.print("Run " + run + " of " + runs);
			
			start = System.currentTimeMillis();
			modPar.execute();
			end = System.currentTimeMillis();
			
			String append = "";
			
			if (runVersion == 0 && run == 1) {
				for (UnsatisfiedConstraint uc : modPar.getContext().getUnsatisfiedConstraints()) {
					String message = uc.getMessage();
					Integer currentCount = singleThreadResult.get(message);
					if (currentCount == null)
						singleThreadResult.put(message, 1);
					else
						singleThreadResult.put(message, currentCount + 1);
				}
			}
			
			if (runVersion > 0) {
				multiThreadResult.clear();
				
				Iterator<UnsatisfiedConstraint> ucs = modPar.getContext().getUnsatisfiedConstraints().iterator();
				while (ucs.hasNext()) {
					String message = ucs.next().getMessage();
					Integer currentCount = multiThreadResult.get(message);
					if (currentCount == null)
						multiThreadResult.put(message, 1);
					else
						multiThreadResult.put(message, currentCount + 1);
				}
				
				if (checkResult(singleThreadResult, multiThreadResult)) {
					System.out.println(" - ok");
					logger.info("Run " + run + " of " + runs + " - ok");
				}
				else {
					append = "!";
					System.out.println(" - error");
					logger.info("Run " + run + " of " + runs + " - error");
				}
			}
			else {
				System.out.println(" - ok");
				logger.info("Run " + run + " of " + runs + " - ok");
			}
			
			if (run == 1)
				ro.append(nl + model.getName() + "," + numThreads + "," + (end - start) + append + ",");
			else if (run < runs)
				ro.append((end - start) + append + ",");
			else
				ro.append((end - start) + append + "");
			
			emfModel.disposeModel();
		}
	}

	static boolean checkResult(HashMap<String, Integer> singleThreadResult,
			HashMap<String, Integer> multiThreadResult) {
		
//		System.out.println("\nIn Check Result");
//		System.out.println("single: " + singleThreadResult.size());
//		System.out.println("multi: " + multiThreadResult.size());
		
		if (singleThreadResult.size() != multiThreadResult.size())
			return false;
		
		String[] results = new String[singleThreadResult.size()];
		singleThreadResult.keySet().toArray(results);
		Arrays.sort(results);
		
		for (int i = 0; i < results.length; i++) {
			
//			System.out.println("\nResult: " + results[i]);
//			System.out.println("Single: " + singleThreadResult.get(results[i]));
//			System.out.println("Multi: " + multiThreadResult.get(results[i]));
					
			if (!singleThreadResult.get(results[i]).equals(multiThreadResult.get(results[i]))) {
				return false;
			}
		}
		return true;
	}
}
