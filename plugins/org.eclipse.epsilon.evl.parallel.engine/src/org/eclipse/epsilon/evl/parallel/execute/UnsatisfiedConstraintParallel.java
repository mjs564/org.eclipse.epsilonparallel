package org.eclipse.epsilon.evl.parallel.execute;

import org.eclipse.epsilon.evl.execute.UnsatisfiedConstraint;

public class UnsatisfiedConstraintParallel extends UnsatisfiedConstraint {

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof UnsatisfiedConstraintParallel))
			return false;
		
		return this.constraint == ((UnsatisfiedConstraintParallel) obj).constraint &&
				this.instance == ((UnsatisfiedConstraintParallel) obj).instance;
	}
	
	@Override
	public int hashCode() {
		return constraint.hashCode() + instance.hashCode();
	}
}