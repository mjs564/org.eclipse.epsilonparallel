package org.eclipse.epsilon.evl.parallel;

import java.util.concurrent.ThreadFactory;

import org.eclipse.epsilon.evl.parallel.execute.context.IEvlContextParallel;

public class EvlModuleParallelThreadFactory implements ThreadFactory {

	private int threadCount = 0;
	private IEvlContextParallel context;
	private volatile EvlModuleParallelBatchData batchData;
	
	public EvlModuleParallelThreadFactory(IEvlContextParallel context, EvlModuleParallelBatchData batchData) {
		this.context = context;
		this.batchData = batchData;
	}
	
	@Override
	public Thread newThread(Runnable r) {
		Thread newThread = new Thread(r) {
			
			@Override
			public void run() {
				synchronized (EvlModuleParallelThreadFactory.this) {
					Thread.currentThread().setName(Integer.toString(threadCount++));
				}
				context.addParallelFrameStack();
				context.addParallelOperationContributorRegistry();
				context.addParallelUnsatisfiedConstraints();
				if (batchData != null)
					batchData.registerThread();
				super.run();
			}
		};

		return newThread;
	}
}