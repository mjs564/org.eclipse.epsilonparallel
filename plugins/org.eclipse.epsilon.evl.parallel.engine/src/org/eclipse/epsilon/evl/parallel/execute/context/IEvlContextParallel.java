package org.eclipse.epsilon.evl.parallel.execute.context;

import java.util.Set;

import org.eclipse.epsilon.evl.dom.Constraint;
import org.eclipse.epsilon.evl.execute.context.IEvlContext;

public interface IEvlContextParallel extends IEvlContext {

	public void goParallel();
	public void endParallel();
	public void addParallelFrameStack();
	public void addParallelOperationContributorRegistry();
	public void addParallelUnsatisfiedConstraints();
	public void setConstraintsDependedOn(Set<Constraint> constraints);
	public Set<Constraint> getConstraintsDependedOn();
}
